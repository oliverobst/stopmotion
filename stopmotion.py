#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 17 12:54:46 2020

@author: oliver
"""

import argparse
import csv
import pathlib
import sys
import cv2
import numpy as np
import textwrap
from PyPDF2 import PdfFileReader
from pdf2image import convert_from_path
from PIL import Image


class OpenFileError(Exception):
    """Exception raised for errors opening files.
    
    Attributes:
        location -- function and context in which the error occured
        path -- the file we tried to open
    """
    def __init__(self, location, path):
        self.location = location
        self.path = path
        
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)
                
class StopMotion:
    
    def __init__(self, videopath = None, filetype = 'avi', filepath = None, 
                 filelistpath = None, n_frames = 240, fps = 24,
                 xres = 1280, yres = 720):
        """
        Parameters
        ----------
        videopath : str, optional
            The filename for the output video. The default is None.
        filetype : str, optional
            The output file type, either 'avi' or 'mp4'. The default is 'avi'.
        filepath : str, optional
            A filename for a single input file. The default is None.
        filelistpath : str, optional
            A filename for a CSV file that lists input files. 
            The default is None.
        n_frames : int, optional
            The default number of frames per page. The default is 240.
        fps : int, optional
            Frames per second for the output video. The default is 24.
        xres : int, optional
            Video width in pixels. The default is 1280.
        yres : TYPE, optional
            Video height in pixels. The default is 720.

        Returns
        -------
        None.

        """
        self.videopath = videopath
        self.filepath = filepath
        self.filelistpath = filelistpath
        self.filelist = None
        self.fps = fps
        self.resample = Image.BICUBIC
        self.n_frames = n_frames  # the default number of reps for a frame 
        self.xres = int(xres)
        self.yres = int(yres)
        self.frames = []     # list of tuples (frame, duration)
        
        if filetype == 'mp4':
            self.filetype = cv2.VideoWriter_fourcc(*'mp4v')
        elif filetype == 'avi':
            self.filetype = cv2.VideoWriter_fourcc(*'XVID')
        else:
            self.filetype = None
            
    def strrange2set(self, s):
        """Convert a range given as string (like "1,2,5-11") into a set.
        
        Parameters
        ----------
        s : str
            A range, given as string. Can contain lists of numbers or ranges.

        Returns
        -------
        result : set
            The string converted into a set, containing all integers
            in ranges (i.e., "1-100" will convert into {1,2,3,...,98,99,100}.
        """
        
        result = set()
        for part in s.split(','):
            if '-' in part:
                a, b = part.split('-')
                a, b = int(a), int(b)
                result.update(range(a, b + 1))
            else:
                a = int(part)
                result.add(a)
        return result    

    def check_inputfiles(self):
        
        if self.filelistpath is not None:
            self.read_filelist(self.filelistpath)
        elif self.filepath is not None:
            file = pathlib.Path(self.filepath)
            if file.exists():
                self.filelist = [(self.filepath, self.n_frames, None)]
            else:
                eprint('File "{}" does not exist, nothing to do.'.format(self.filepath))
        else:
            eprint('No input files, nothing to do.')

    def read_filelist(self, filelistpath):
        """Read in file with filenames, number of frames, and page numbers.
        
        This reads in the list as a (whitespace separated) values. This will
        be converted into a list of 3-tuples, with each first element an 
        existing file, seconds elements state the number of frames a page
        from that file should be visible, and the 3rd element is a range
        of pages to be used, or None (= use all pages).
        This list is stored in the filelist member variable.

        Parameters
        ----------
        filelistpath : filename
            The filename of the CSV (Whitespace-separated values) file.

        Returns
        -------
        None.

        """
        
        # the file format is a CSV file, with up to 3 relevant columns:
        # - file name of a PDF
        # - number of frames (int). OPTIONAL: use default_frames if not given.
        # - a range of page numbers, OPTIONAL. Use all pages if not given. A
        #   combination of single pages, e.g., 1,2,3, or ranges, e.g., 1-3.
        
        with open(filelistpath, newline='') as f:
            reader = csv.reader(f, delimiter=' ')
            data = [tuple(row) for row in reader]
            self.filelist = list()
            for t in data:
                if len(t) == 0:
                    continue

                if len(t) >= 3:
                    pages = self.strrange2set(t[2])
                else:
                    pages = None

                if len(t) >= 2:
                    n_frames = t[1]
                else:
                    n_frames = self.n_frames
                    
                file = pathlib.Path(t[0])
                if file.exists():
                    self.filelist.append((t[0], int(n_frames), pages))
                else:
                    eprint('File "{}" does not exist, skipped.'.format(t[0]))        
        
    def read_frames(self):
        for filename, n_frames, selection in self.filelist:
            pdffile = PdfFileReader(open(filename, 'rb'))
            n = pdffile.getNumPages()
            if selection is None:
                selection = set(range(1,n+1))
            else:
                selection &= set(range(1,n+1))
                
            for p in selection:
                # calculate size to that the pdf fills the movie without crop
                width = pdffile.getPage(p-1).mediaBox.getWidth()
                height = pdffile.getPage(p-1).mediaBox.getHeight()
                scale = min(self.xres / width, self.yres / height)
                width = int(width * scale)
                height = int(height * scale)
                # now load the page in the designated size 
                pdfimage = convert_from_path(filename, size=(width, height), 
                                             first_page = p, last_page = p)
                if pdfimage:
                    self.frames.append((pdfimage[0], n_frames, (width, height)))
            
            pdffile = None


    def convert_resize(self, image, size):
        """Convert image into np.array in BGR color, add border if necessary.
        
        Convert from RGB into BGR colour space. Translate into np.array.
        Finally, add a black border around the image if necessary to bring 
        the image to full video frame size.

        Parameters
        ----------
        image : PIL Image
            A pixel image, smaller or equal to the designated video frame size.
        size : a tuple of two int
            (width, height) of the image as it is.

        Returns
        -------
        image : np.array
            The converted image, in BGR color order.

        """        
        
        border_colour = [int(0), int(0), int(0)]
        image = cv2.cvtColor(np.asarray(image), cv2.COLOR_RGB2BGR)            

        # add a border around the image if necessary
        lborder = self.xres - size[0]
        tborder = self.yres - size[1]
        
        if lborder > 0 or tborder > 0:
            rborder = int(lborder/2)
            bborder = int(tborder/2)
            lborder = self.xres - size[0] - rborder
            tborder = self.yres - size[1] - bborder
            image = cv2.copyMakeBorder(image, tborder, bborder, lborder, 
                                       rborder, cv2.BORDER_CONSTANT, None, 
                                       border_colour)
            
        return image

    def write_video(self):
        out = cv2.VideoWriter(self.videopath, apiPreference=cv2.CAP_FFMPEG,
                              fourcc=self.filetype, 
                              fps=self.fps,
                              frameSize=(self.xres, self.yres))
        
        for image, n_frames, size in self.frames:            
            image = self.convert_resize(image, size)            
            for t in range(n_frames):
                out.write(image)
            
        out.release()
        
class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter, 
                      argparse.RawDescriptionHelpFormatter):
    pass
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=CustomFormatter,
        description=textwrap.dedent('''\
        Convert PDF(s) into a movie.
        The PDF pages are rendered in the resolution of the video, without clipping.
        If the aspect ratio of the PDF is different from the video, a border will
        be added on either both sides, or top and bottom, with the content centered.
        The output format will depend on the chosen file suffix (avi or mp4).
        '''),
        epilog = textwrap.dedent('''\
        It may be useful to post-process the output with ffmpeg.
        
        The file containing the list of input files can list for each input 
        - the file name (a relative or absolute path to a PDF),
        - the number of frames pages in the PDF are shown,
        - the list of pages to select from the PDF (starts with page 1, use all pages if not given)
        These three items should be separated by space. File names with spaces
        should be quoted. The number of frames applies to each selected page
        in the PDF, and both number of frames and list of pages are optional.
        '''))
    
    parser.add_argument('output', type=str, 
                        help='filename for the video, should end in .mp4 or .avi')
    parser.add_argument('--input', nargs = '?', const = '', default = None, 
                        type = str,
                        help='Single input file. Mutually exclusive with --list')
    parser.add_argument('--list', nargs = '?', const = '', default = None, 
                        type = str,
                        help='File containing a list of input files. Mutually exclusive with --input')
    parser.add_argument('--nframes', type = int, default = 240,
                        help='The number of frames for each page in the PDFs, if not specified in the input list of files.')
    parser.add_argument('--fps', type = int, default = 24,
                        help='Number of frames/sec in the output video.')
    parser.add_argument('--xres', type = int, default = 1280,
                        help='Width (x resolution) of the output video.')
    parser.add_argument('--yres', type = int, default = 720,
                        help='Height (y resolution) of the output video.')
   
    args = parser.parse_args()

    success = False
    message = ''
    while not success:
        filetype = None
        if args.output[-3:] == 'mp4' or args.output[-3:] == 'avi':
            filetype = args.output[-3:]
        else:
            message = 'Unsupported output file type'
            break
        if args.input is None and args.list is None:
            message = 'No input file(s)'
            break
        if args.input is not None and args.list is not None:
            eprint('Both input file and input list specified. Ignoring input file.')
            args.input = None
        if args.fps < 1:
            message = 'FPS must be greater than 0'
            break
        if args.xres < 10 or args.yres < 10:
            message = 'Video resolution too small (xres, yres should be >= 10)'
            break
        
        sm = StopMotion(videopath = args.output, filetype = filetype,
                        filepath = args.input,
                        filelistpath = args.list,
                        n_frames = args.nframes,
                        fps = args.fps,
                        xres = args.xres, yres = args.yres)

        sm.check_inputfiles()
        sm.read_frames()
        sm.write_video()
        success = True

    if not success:
        eprint(message)
        
